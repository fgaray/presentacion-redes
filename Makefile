pdf: clear
	pandoc presentacion.md --slide-level 2 -t beamer -o talk.tex
	pdflatex -shell-escape main.tex
	biber main
	pdflatex -shell-escape main.tex


clear:
	rm -f main.aux main.bbl main.bcf main.blg main.log main.nav main.out main.pdf main.run.xml main.toc main.vrb main.snm
