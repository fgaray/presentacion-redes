# Introducción
## ¿Qué es BitTorrent?

BitTorrent es un protocolo peer-to-peer \footnote{Las redes peer-to-peer
funcionan sin clientes ni servidores fijos sino que los nodos se comportan como
si fueran todos iguales} diseñado por Bram Cohen para facilitar
la transferencia de archivos a lo largo de múltiples pares a través de redes no
confiables (\cite{Theory:Specification}).


\begin{figure}[t]
	\centering
	\includegraphics[scale=1]{imagenes/bittorrent-logo.png}
	\caption{Logo de BitTorrent, Inc.}
\end{figure}




#Componentes importantes
## Swarm y piece
* BitTorrent permite que varios usuarios se conecten 
para formar agrupaciones llamadas swarm (enjambre), a la hora de subir o 
descargar un archivo de interés común, simultáneamente.
* Para distribuir el archivo este se divide en partes pequeñas denominadas
pieces, asociada a un identificador hash.


## Seed y leecher
Dentro de un swarm se distinguen dos tipos de nodos:

* Seed: son los que posee todas las partes del archivo y que actúa como servidor
en la distribución.
* Leecher: son los nodos interesados en tener el archivo distribuido, puede tener
o no partes del archivo, si posee algunas, puede ser un servidor ofreciendo
aquellas partes.


## Tracker
Servidor especial que organiza la distribución del archivo, contiene toda la 
información necesaria para conectar a los nodos peer formando un swarm según
el protocolo BitTorrent. Los nodos clientes deben conectarse a él para 
poder iniciar descarga (\cite{Xatakaon}).

<!---
Comentario
-->

## Funcionamiento básico

BitTorrent utiliza el protocolo TCP como transporte. Utiliza los puertos
6881-6889 y el 6969 para el tracker. Además hay una extensión al protocolo
llamada DHT que utiliza varios puertos UDP. (\cite{Wireshark:Torrent})


<!---
Los puertos UDP se negocian cada vez que se inicia sesión en el protocolo
-->


# Funcionamiento
## Inicio

Al inicio, un cliente lee la información desde un archivo .torrent el cual
contiene una dirección de un tracker común para todos los otros clientes que
quieren obtener el archivo. El cliente realiza una petición HTTP al tracker
para obtener los otros pares que el conoce. Además el .torrent contiene
metadatos necesarios para bajar el archivo y un hash (info_hash) SHA1 que
identifica al torrent.

<!---
BitTorrent no provee indexamiento por lo que hay otros servicios que dan la
posibilidad de bajar archivos .torrent
-->


## Estructura del archivo .torrent



\begin{table}[t]
\begin{tabularx}{\textwidth}{|c|X|}
        \hline
        Campos & Contenido \\ \hline \hline

        info   & Contiene la estructura de los archivos a transferir como los
        bloques que los componen, un hash md5 para comprobarlos y el nombre \\ \hline

        announce & Una lista de los trackers a usar (urls) \\ \hline


        
\end{tabularx}
\end{table}


<!---


info: a dictionary that describes the file(s) of the torrent. There are two
possible forms: one for the case of a 'single-file' torrent with no directory
structure, and one for the case of a 'multi-file' torrent (see below for
details)

announce: The announce URL of the tracker (string)
announce-list: (optional) this is an extention to the official specification, offering backwards-compatibility. (list of lists of strings).
        
creation date: (optional) the creation time of the torrent, in standard UNIX epoch format (integer, seconds since 1-Jan-1970 00:00:00 UTC)
comment: (optional) free-form textual comments of the author (string)
created by: (optional) name and version of the program used to create the .torrent (string)
encoding: (optional) the string encoding format used to generate the pieces part of the info dictionary in the .torrent metafile (string) 
-->




## Formato de los mensajes

Los mensajes utilizan un formato llamado bencoding que permite organizar los
datos de una forma parecida al formato JSON. Soporta cadenas de bytes, enteros,
listas y diccionarios.

### Ejemplos

"4:spam" representa la cadena "spam" 

"i3e" representa el entero "3" 

"l4:spam4:eggse" representa la lista ["spam", "eggs"]

"d3:cow3:moo4:spam4:eggse" representa el diccionario { "cow" => "moo", "spam" => "eggs" } 




## Obtener los pares

El tracker es un servicio HTTP que responde a peticiones GET. Cada petición
incluye métricas que ayudan al tracker a mantener estadísticas sobre sobre el
torrent. La respuesta incluye una lista de pares que ayudan a cliente a
participar en la compartición de archivos.


Los parámetros son pasados igual que en cualquier página web.




## Respuesta del tracker

\begin{table}[t]
\begin{tabularx}{\textwidth}{|c|X|}
        \hline
        Campos & Contenido \\ \hline \hline

        peers & Lista de pares que se encuentran compartiendo o bajando el
        torrent solicitado. Esta lista incluye las ips y puertos en los que
        están escuchando los pares\\ \hline

        complete & Número de pares que tienen el archivo completo \\ \hline
        incomplete & Número de pares que tienen el archivo incompleto \\ \hline
        
\end{tabularx}
\end{table}





## Handshake

Para intercambiar mensajes entre pares primero se debe realizar un saludo
(handshake). El par que inicia la conexión envía su handshake. En el lado del
par que recibe se verifica si el info_hash corresponde a un torrent que esta
siendo servido por el. Si no es así entonces se descarta el mensaje.



<!---
strlen: string length of <pstr>, as a single raw byte

pstr: string identifier of the protocol

reserved: eight (8) reserved bytes. All current implementations use all zeroes.
Each bit in these bytes can be used to change the behavior of the protocol. An
email from Bram suggests that trailing bits should be used first, so that
leading bits may be used to change the meaning of trailing bits.

info_hash: 20-byte SHA1 hash of the info key in the metainfo file. This is the same info_hash that is transmitted in tracker requests.

peer_id: 20-byte string used as a unique ID for the client. This is usually the
same peer_id that is transmitted in tracker requests (but not always e.g. an
anonymity option in Azureus). 

In version 1.0 of the BitTorrent protocol, pstrlen = 19, and pstr = "BitTorrent protocol". 
-->



## Paso de mensajes

Existen cuatro mensajes que se utilizan para pedir bloques de archivos a otros
pares, estos son: "Choke", "Unchoke", "Interested" y "Not Interested"


<!---
‘Interested’ means that the downloading client (that’s you) would like to
download from the peer. ‘Choke’ means that the peer serving the file will not
send it to you until they ‘Unchoke’ you. All connections start off as ‘Not
Interested’ and ‘Choked’. In order to get to a state where you can receive
files, you need to send your peer an ‘Interested’ message, and they need to send
you an Unchoke message. You should wait for this Unchoke message from your peer
before requesting pieces. Once you are unchoked, a client can still send you a
Choke message at any time, at which point you should refrain from requesting
pieces from that peer.

Unchoke significa que vamos a servir al cliente 
-->


## Paso de mensajes

Una vez que se envía un mensaje "Interested" y se recibe un mensaje "Unchoke"
se puede empezar a pedir bloques. El mensaje de petición "Request" lleva el
indice del bloque que se quiere bajar. Generalmente se piden aquellos bloques
más raros primero (aquellos que se encuentran en pocos pares).



## Paso de mensajes - Respuesta

Un par debe responder con un mensaje que contiene el bloque pedido. Si se quiere
servir los bloques que se van recibiendo (como todos los clientes torrents), se
debe enviar un mensaje "Have" a todos los pares conectados luego de comprobar
por medio de un hash que el bloque llegó correctamente.



# Ejemplo de transferencia de archivos
## Bajar archivo .torrent


\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/bajar_torrent.png}
\end{figure}


## Anunciarse con el tracker

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/get_announce.png}
\end{figure}


## Respuesta con los pares

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/response_announce.png}
\end{figure}


## Handshake

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/handshake.png}
\end{figure}


## Interested

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/interested.png}
\end{figure}




## Choke/Unchoke

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/choke.png}
\end{figure}


## Request

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/request.png}
\end{figure}


## Transferencia de bloques

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/respuesta_bloque.png}
\end{figure}


## General

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{imagenes/general.png}
\end{figure}



# Extensión de protocolo DHT
## DHT

BitTorrent utiliza una tabla hash distribuida (DHT) para guardar la información
de contacto de cada uno de los pares (ip, torrents). Gracias a esta extensión,
cada par se convierte en un tracker sin necesidad de un servidor centralizado.
(\cite{DHT})


<!---
El protocolo DHT se basa en el protocolo de kademila
-->


## Funcionamiento

Cada nodo tiene un identificador único llamado "node ID" los cuales son
escogidos de forma aleatoria de un espacio de 160 bits. Luego se calcula
utilizando la función "xor" la distancia con otros ids. Un nodo va a tener
información de aquellos nodos que se encuentren cerca en una tabla de rutas.


## Encontrar peers

Cuando un nodo quiere encontrar pares para descargar un archivo calcula la
distancia del info_hash con la de los nodos que se encuentran en su tabla de
rutas. Luego pregunta a los nodos cercanos que le busquen algún nodo que este
sirviendo el torrent. Si algún nodo sabe sobre algún par que este sirviendo el
torrent responde con la información de contacto, en caso contrario se responde
con los nodos en la tabla de rutas.

#Ventajas y problemas con BitTorrent
## Ventajas

* No saturar servidores: quien provee el archivo, se ve beneficiado por el 
ancho de banda de los clientes que van descargando partes del archivo.
* Redundancia: si por algún motivo un servidor se cae, mientras hayan nodos seed
no parará la distribución del archivo.
* Alta disponibilidad a la hora de descargar: siempre hay muchos usuarios 
conectados a los cuales poder descargarles una piece del archivo.

## Problemas

* Dependencia de los peers: si los usuarios que poseen el archivo o parte de
él, no se encuentran conectados, el protocolo no tiene sentido.
* Dependencia del tracker: este es quien organiza toda la distribución, por lo
tanto para impedir la transmisión es tan simple como dejar fuera de servicio al 
tracker. 
* Errores o trampas: algunas empresas para introducir errores o sabotear la 
transferencias incluyen ficheros falsos.







# Conclusión
## Conclusión

El protocolo BitTorrent es una forma eficiente para distribuir archivos a varios
clientes en internet con una pequeña sobrecarga en el seeder original. 


Aunque es famoso por ser utilizado para bajar películas o música también tiene usos
legítimos como en el servicio de almacenamiento S3 de Amazon, distribuir
actualizaciones de juegos online como WoW, Diablo III y EVE Online.  Además
Facebook utiliza este protocolo para distribuir actualizaciones entre sus
servidores.

Existe un programa de computación distribuida que utiliza el protocolo para
distribuir datos científicos entre sus usuarios.






# Referencias
## Referencias

\printbibliography[heading=none]



## Gracias

\begin{center}
\LARGE{¿Preguntas?}
\end{center}
